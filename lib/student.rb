

class Student

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_reader :first_name, :last_name

  def courses
    @courses
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    @courses.each do |course|
      if course.conflicts_with?(new_course)
        raise
      end
    end

    @courses << new_course unless @courses.include?(new_course)
    new_course.students << self

  end

  def course_load
    course_loads = Hash.new(0)
    self.courses.each do |course|
      course_loads[course.department] += course.credits
    end
    course_loads
  end



end
